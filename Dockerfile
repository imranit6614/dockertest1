#specify a base package
FROM node:alpine

WORKDIR '/app'

COPY package.json .
#install dependencies

RUN npm install

COPY . .

#Default command

CMD ["npm" ,"start"]